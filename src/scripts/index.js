// Load styles
import Foundation from 'foundation-sites';
import '../../src/styles/global.scss';
import '../../src/styles/fonts.scss';
import "../../src/modules/research-insights/research-insight.scss";
import "../../src/modules/advocacy/advocacy.scss";
import "../../src/modules/subscribe/subscribe.scss";
import "../../src/modules/take-action/take-action.scss";
import "../../src/modules/our-methodology/our-methodology.scss";
