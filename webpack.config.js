const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
      app: './src/scripts/index.js'
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/modules/index/index.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'research-insights.html',
      template: './src/modules/research-insights/research-insights.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'advocacy.html',
      template: './src/modules/advocacy/advocacy.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'subscribe.html',
      template: './src/modules/subscribe/subscribe.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'take-action.html',
      template: './src/modules/take-action/take-action.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'our-methodology.html',
      template: './src/modules/our-methodology/our-methodology.html'
    }),
    new CopyPlugin([
      {from: './src/modules/subscribe/images', to: 'images'},
      {from: './src/modules/take-action/images', to: 'images'},
      {from: './src/modules/our-methodology/images', to: 'images'}
  ])
  ],
  // devtool: 'source-map',
  devServer: {
    contentBase: './dist'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
     module: {
         rules: [
          {
            test: /\.scss$/,
            use: [
              {
                loader: "style-loader"
              }, 
              {
                loader: "css-loader"
              }, 
              {
                loader: "sass-loader"
              }
            ]      
          },
            //  Loads images
           {
             test: /\.(png|svg|jpg|gif)$/,
             use: [
              {
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]',
                  outputPath: 'resources/'
                }
              }
            ]
          },
           // Loads fonts
           {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]',
                  outputPath: 'fonts/'
                }
              }
            ]               
           }      
         ]
       }
};